//#include <OneWire.h>
//#define ONEWIRE_CRC8_TABLE 1

#include <WiFi.h>
#include <WiFiUdp.h>
//#include <SPI.h>
#include <ESP32DMASPIMaster.h>

ESP32DMASPI::Master master;

static const uint32_t BUFFER_SIZE = 8;
uint8_t* spi_master_tx_buf;
uint8_t* spi_master_rx_buf;


//unsigned int  localPort = 1900;
const char* ssid = "PLIS";
const char* pass = "12344321";

//IP address to send UDP data to:
// either use the ip address of the server or 
// a network broadcast address
const char * udpAddress = "192.168.0.255";
const int udpPort = 3333;

IPAddress softAPIP;


const char* buffer_data = "";
const uint8_t countByteInAdrr = 6;
const uint8_t countAdrr = 70;

WiFiUDP Udp;

//Are we currently connected?
boolean connected = false;

const int chipSelectPin = 15;
//uint8_t receivedSPIbyte = 0;
uint8_t UDPPacket[1024];

bool ConnectWiFi(const char *ssid, const char *pass);
uint16_t getDataFromSPI(uint8_t *prtUDPPacket);
void WiFiEvent(WiFiEvent_t event);

void setup()
{
  Serial.begin(115200);// запуск последовательного порта
  //Serial.setDebugOutput(true);
  Serial.println();
  Serial.print("Configuring access point...");

    WiFi.mode( WIFI_STA);             // режим клиента
  //WiFi.mode( WIFI_AP_STA);             // режим клиента и точки доступа
  /* You can remove the password parameter if you want the AP to be open. */
 // WiFi.softAP(ssid, pass);
  //wifi_softap_dhcps_stop();

  // Соединение с WiFi
  ConnectWiFi(ssid,pass);

  Udp.begin(udpPort);          // открываем порт
   
  /*настройкав SPI на режим DMA*/
      // to use DMA buffer, use these methods to allocate buffer
    spi_master_tx_buf = master.allocDMABuffer(BUFFER_SIZE);
    spi_master_rx_buf = master.allocDMABuffer(BUFFER_SIZE);

    master.setDataMode(SPI_MODE3); // for DMA, only 1 or 3 is available
    master.setFrequency(SPI_MASTER_FREQ_8M);
    master.setMaxTransferSize(BUFFER_SIZE);
    master.setDMAChannel(1); // 1 or 2 only
    master.setQueueSize(1); // transaction queue size
    
    // begin() after setting
    // HSPI = CS: 15, CLK: 14, MOSI: 13, MISO: 12
    master.begin(); // default SPI is HSPI
  
//  pinMode(chipSelectPin, OUTPUT);
//  digitalWrite(chipSelectPin, HIGH);

}

uint16_t cnt;

void loop()
{

	cnt = 0;

	//if (WiFi.status() == WL_CONNECTED)
	if(connected){
		cnt = getDataFromSPI(UDPPacket);
		delay(10);
	}
	//    cnt = 100; //??????????????
	if (cnt){ //  готовим UDP пакет только если принят хоть один 48битный регистр
		unsigned int packetSize = Udp.beginPacket(udpAddress, udpPort);
		if (packetSize)  //устанавливаем соединение с удаленным устройством
		{
			Udp.write(UDPPacket,cnt);
			Udp.endPacket();
			//Udp.stop();
		}
		delay(1000);
	}


}

/*########################################################
  Соединение с WiFi
*/
bool ConnectWiFi(const char *ssid, const char *pass)
{
    // We start by connecting to a WiFi network

    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    // delete old config
    WiFi.disconnect(true);
    //register event handler
    WiFi.onEvent(WiFiEvent);

    WiFi.begin(ssid, pass);
//
//    while (WiFi.status() != WL_CONNECTED) {
//        delay(500);
//        Serial.print(".");
//    }
//
//    Serial.println("");
//    Serial.println("WiFi connected");
//    Serial.println("IP address: ");
//    Serial.println(WiFi.localIP());

//    }
//  }
	return false;
}

//wifi event handler
void WiFiEvent(WiFiEvent_t event){
    switch(event) {
      case SYSTEM_EVENT_STA_GOT_IP:
          //When connected set 
          Serial.print("WiFi connected! IP address: ");
          Serial.println(WiFi.localIP());  
          //initializes the UDP state
          //This initializes the transfer buffer
          Udp.begin(WiFi.localIP(),udpPort);

          softAPIP = WiFi.softAPIP();
          Serial.print("AP IP address: ");
          Serial.println(softAPIP);
          connected = true;
              Udp.beginPacket(udpAddress,udpPort);
              Udp.printf("Seconds since boot: %lu", millis()/1000);
              Udp.endPacket();
          break;
      case SYSTEM_EVENT_STA_DISCONNECTED:
          Serial.println("WiFi lost connection");
          connected = false;
          break;
      default: break;
    }
}



uint16_t getDataFromSPI(uint8_t *prtUDPPacket)
{
//  uint16_t k;
//  uint8_t cnt_FFbyte = 0;
//
//  SPI.beginTransaction(SPISettings(20000000, MSBFIRST, SPI_MODE0));
//  yield();
//  k = 0;
//  while (k < countAdrr * countByteInAdrr) //==количество переданных байт
//  {
//    //      noInterrupts();
//    //  delayMicroseconds(5);
//    prtUDPPacket[k] = SPI.transfer(0x00);
//
//    if (prtUDPPacket[k] == 0xff)
//    {
//      cnt_FFbyte++;
//    }
//
//    k++;
//
//    //обнаружен конец пакета(шесть подряд FF) => выходим
//    if (cnt_FFbyte == countByteInAdrr)
//    {
//      if (k == countByteInAdrr) // ЗНАЧИТ, что передеча данных еще не начиналась
//      {
//        k = k - countByteInAdrr; // не фиксируем приход 48битного регистра FF
//      }
//      yield();
//      break;
//    }
//
//    //     if (k==1) Serial.print("Start Data");
//    if ((k % countByteInAdrr) == 0) // сбрасываем кадждый 6 байт
//    {
//      cnt_FFbyte = 0;
//      //delay(10);
//      //delayMicroseconds(15);
//      //          interrupts();
//      yield();
//    }
//  }
//
//  SPI.endTransaction();

//  return k;

    // start and wait to complete transaction
    master.transfer(spi_master_tx_buf, spi_master_rx_buf, BUFFER_SIZE);

    // do something here with received data (if needed)
    for (size_t i = 0; i < BUFFER_SIZE; ++i)
        printf("%d ", spi_master_rx_buf[i]);
    printf("\n");


}
